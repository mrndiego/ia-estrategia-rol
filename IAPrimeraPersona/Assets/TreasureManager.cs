using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureManager : MonoBehaviour
{
    bool available = true;
    public GameObject treasure;
    public bool Steal()
    {
        if (available)
        {
            available = false;
            StartCoroutine("WaitForRespawn");
            treasure.SetActive(false);
            return true;
        }
        return false;
    }
    IEnumerator WaitForRespawn()
    {
        yield return new WaitForSeconds(10);
        available = true;
        treasure.SetActive(true);
    }
}

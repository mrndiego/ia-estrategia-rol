using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseState      // Estado base del que heredan los dem�s estados.
{
    public virtual void Execute(WandererSM stateMachine) { }    // M�todo que implementar�n todos los estados
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WandererSM : MonoBehaviour                         //M�quina de estados del agente Wanderer
{
    BaseState CurrentState;                                     // Estado actual de la m�quina de estados
    public readonly BaseState Walking = new WalkingState();     // Estado 'caminando'
    public readonly BaseState Resting = new RestingState();     // Estado 'descansando en un banco'

    // Par�metros que modifican el comportamiento del estado Walking
    public float radius = 25;
    public float offset = 5;

    // Lista de bancos en los que se puede sentar el agente
    public Transform[] Bancos;

    public NavMeshAgent Agent;          // Referencia al NavMeshAgent del propio agente

    public bool Descansado = true;      // Variable que refleja si el agente necesita descansar
    public int ContadorPasos = 0;       // Contador de movimientos que ha hecho el agente. 

    public float TimeObjetivo = -1f;    // Variable auxiliar que se utilizar� en el estado Resting.

    void Start()
    {
        Agent = GetComponent<NavMeshAgent>();   // Guardamos la referencia del agente
        Agent.speed = 10;                       // Establecemos su velocidad de movimiento
        CurrentState = Walking;                 // Establecemos el estado inicial
    }

    void Update()
    {
        if (!Agent.pathPending && Agent.remainingDistance < 0.5f)   
            //Si el agente ha terminado de desplazarse y est� a menos de 0.5 unidades de distancia de su objetivo
        {
            CurrentState.Execute(this); // Ejecutamos la acci�n que corresponda con el estado actual
        }
    }

    public void Transition(BaseState state) // M�todo auxiliar que cambia el estado actual al indicado
    {
        CurrentState = state;
    }



}

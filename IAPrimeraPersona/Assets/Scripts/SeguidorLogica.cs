using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SeguidorLogica : MonoBehaviour
{

    private NavMeshAgent agent;                 // El propio agente.

    public NavMeshAgent objetivo;               // El agente al que seguir.

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();   // Obtenemos el componente NavMeshAgent del FollowerRunner.
        agent.speed = objetivo.speed * .9f;     // Asignamos su velocidad al 90% de la del GhostRunner.
    }
    void Update()
    {
        agent.destination = objetivo.GetComponent<Transform>().position;    // Cambiamos el destino del FollowerRunner a la posici�n actual del GhostRunner.
    }
}

using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class Runner : MonoBehaviour
{

    public Transform[] points;      // Array de destinos, la ruta a seguir.
    
    // Variables internas del corredor.
    private int destPoint = 0;
    private NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();               // Obtenemos el componente NavMeshAgent del GhostRunner.
        agent.speed = Random.Range(1.0f, 15.0f);            // Asignamos una velocidad aleatoria.
        GotoNextPoint();                                    // Iniciamos la patrulla.
    }

    void GotoNextPoint()
    {
        if (points.Length == 0) return;                     // Si la lista de destinos est� vac�a retornamos.
        agent.SetDestination(points[destPoint].position);   // Asigna el destino actual.
        destPoint = (destPoint + 1) % points.Length;        // Selecciona el siguiente destino, volviendo al principio del array si fuese necesario.
    }

    void Update()
    {
        if (!agent.pathPending && agent.remainingDistance < 0.5f)   // Si el agente est� cerca de su destino
            GotoNextPoint();                                        // Elige el siguiente destino
    }
}
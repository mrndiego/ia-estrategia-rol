using UnityEngine;

public class TaiChiFormation : MonoBehaviour
{
    public TaichiLeader leader;
    public GameObject followerPrefab;
    public int row1;    // N�mero de agentes en la fila 1
    public int row2;    // N�mero de agentes en la fila 2
    public int row3;    // N�mero de agentes en la fila 3

    void Start()
    {
        createRow(row1, -2f);       // Crear la primera fila con row1 agentes posicionados 2 unidades detr�s del L�der.
        createRow(row2, -4f);       // Crear la segunda fila con row2 agentes posicionados 4 unidades detr�s del L�der.
        createRow(row3, -6f);       // Crear la tercera fila con row3 agentes posicionados 6 unidades detr�s del L�der.
    }

    void createRow(int num, float z)
    {
        float pos = 1 - num;        // Posici�n del agente m�s a la izquierda de la fila.
        for (int i = 0; i < num; ++i)
        {
            Vector3 position = leader.transform.TransformPoint(new Vector3(pos, 0f, z));    // Obtener la posici�n relativa al L�der.
            GameObject temp = (GameObject)Instantiate(followerPrefab, position, leader.transform.rotation); // Instanciar los agentes.
            temp.AddComponent<TaichiFollower>();    // A�adir el script correspondiente al agente Seguidor.
            temp.GetComponent<TaichiFollower>().pos = new Vector3(pos, 0, z-3); // Establecer la posici�n relativa al L�der para el agente.
            temp.GetComponent<TaichiFollower>().leader = leader;    // A�adir la referencia al agente L�der.
            pos += 2f;  // Incrementar la posici�n, para que cada agente est� a la derecha del anterior.
        }
    }
}
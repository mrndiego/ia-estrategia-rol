using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class RestingState : BaseState   // Estado 'descansando en el banco'
{
    private bool move = false;  // Variable que controla si el agente debe moverse

    public override void Execute(WandererSM stateMachine)
    {

        stateMachine.Descansado = true;     // El agente ya no est� cansado
        stateMachine.ContadorPasos = 0;     // Reiniciamos el contador de movimientos

        if (!stateMachine.Agent.pathPending && !move) {     // Si el agente ha terminado de moverse y debe moverse
            stateMachine.TimeObjetivo = Time.time + 5.0f;   // Fijamos el tiempo que debe estar descansando
            move = !move;                                   // Indicamos al agente que no debe moverse
        }

        if (Time.time >= stateMachine.TimeObjetivo && !stateMachine.Agent.pathPending)
            // En el momento que el agente est� quieto y haya pasado el tiempo designado
        {
            stateMachine.Transition(stateMachine.Walking);  // Cambiamos al estado Walking
            move = !move;                                   // Indicamos al agente que debe moverse
        }
    }
}

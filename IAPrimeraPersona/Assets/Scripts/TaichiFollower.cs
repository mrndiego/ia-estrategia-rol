using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TaichiFollower : MonoBehaviour
{
    public NavMeshAgent agent;  // Referencia al NavMeshAgent del Seguidor
    public TaichiLeader leader; // Referencia al Lider
    public Vector3 pos;         // Posicion respecto al Lider, se asigna al instanciar al Seguidor.
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();   // Guardamos la referencia del agente
        transform.position = leader.transform.TransformPoint(pos);  // Colocar el agente en la posicion relativa al Lider
    }
    void Update()
    {
        agent.destination = leader.transform.TransformPoint(pos);        // Desplazar al agente a la posicion correspondiente, relativa al Lider
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockManager : MonoBehaviour
{

    public GameObject[] Abejas;
    public GameObject AbejaPrefab;

    [Header("Bee Settings")]
    [Range(1,100)]
    public int numAbejas = 10;
    [Range(0.1f,5f)]
    public float minSpeed = 1;
    [Range(0.1f, 5f)]
    public float maxSpeed = 5;
    [Range(0f, 10f)]
    public float neighbourDistance = 5;
    [Range(0.1f, 5f)]
    public float rotationSpeed = 3f;


    void Start()
    {
        Abejas = new GameObject[numAbejas];
        for (int i = 0; i < numAbejas; ++i)
        {
            Vector3 pos = this.transform.position + new Vector3(Random.Range(-15, 15), 0, Random.Range(-15, 15)); // Elegir una posicin aleatoria cercana a la colmena.
            Abejas[i] = (GameObject)Instantiate(AbejaPrefab, pos, Quaternion.identity);
            Abejas[i].GetComponent<Flock>().Manager = this;
        }
    }


    void OnDrawGizmosSelected()
    {
        // Debug: muestra un cuadrado en el �rea en la que pueden spawnear las abejas
        Gizmos.color = new Color(0,0,1,.5f);
        Gizmos.DrawCube(transform.position, new Vector3(30,5,30));
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TaichiLeader : MonoBehaviour
{
    public enum Move    // Enumeracion de los movimientos que puede realizar el agente Lider
    {
        Left,
        Right,
        Forward,
        Backward
    }

    public Move LastMove;   // Ultimo movimiento realizado por el Lider

    NavMeshAgent agent;     // Referencia al NavMeshAgent del Lider

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();   // Guardamos la referencia del agente
        agent.updateRotation = false;           // Bloqueamos la rotaci�n del agente. De esta forma se evita que los Seguidores roten alrededor del L�der.
    }

    void Update()
    {
        NextAction();   // En cada frame, intentar realizar una accion
    }
    void NextAction()
    {
        if (!agent.pathPending && agent.remainingDistance < 0.1f)   // Si el agente ha terminado el movimiento de la accion anterior
        {
            switch (Random.Range(0, 4)) // Obtenemos un numero en 0-3, este numero determina que accion realizara el agente.
            {
                case 0:
                    MoveLeft();
                    break;
                case 1:
                    MoveRight();
                    break;
                case 2:
                    MoveForward();
                    break;
                case 3:
                    MoveBackward();
                    break;
            }
        }
    }

    void MoveLeft()
    {
        LastMove = Move.Left;       // Almacenamos la �ltima acci�n del L�der para que los Seguidores puedan imitarla.
        agent.SetDestination(transform.position + new Vector3(-2, 0, 0));     // Moverse a la izquierda.
    }

    void MoveRight()
    {
        LastMove = Move.Right;      // Almacenamos la �ltima acci�n del L�der para que los Seguidores puedan imitarla.
        agent.SetDestination(transform.position + new Vector3(2, 0, 0));    // Moverse a la derecha.
    }
    void MoveForward()
    {
        LastMove = Move.Forward;    // Almacenamos la �ltima acci�n del L�der para que los Seguidores puedan imitarla.
        agent.SetDestination(transform.position + new Vector3(0, 0, 2));    // Moverse hacia delante.
    }
    void MoveBackward()
    {
        LastMove = Move.Backward;   // Almacenamos la �ltima acci�n del L�der para que los Seguidores puedan imitarla.
        agent.SetDestination(transform.position + new Vector3(0, 0, -2));   // Moverse hacia atr�s.
    }
}

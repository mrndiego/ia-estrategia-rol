using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour
{
    public FlockManager Manager;
    public float speed = 1f;

    public Vector3 direction;
    bool first = true;
    private void Start()
    {
        speed = Random.Range(Manager.minSpeed, Manager.maxSpeed);
    }

    private void Update()
    {
        if (first)
        {
            DoFlockMovement();
            first = false;
        }
        if (Random.Range(0, 100) < 10)
        {
            speed = Random.Range(Manager.minSpeed, Manager.maxSpeed); 
        }
        if (Random.Range(0, 100) < 10)
        {
            DoFlockMovement();
        }
        transform.Translate(0.0f, 0.0f, Time.deltaTime * speed);
        transform.position = new Vector3(transform.position.x, 2.5f, transform.position.z);
    }

    void DoFlockMovement()
    {
        GameObject[] bees;
        bees = Manager.Abejas;

        Vector3 cohesion = Vector3.zero;
        Vector3 separation = Vector3.zero;
        float align = .01f;
        float dist;
        int numBees = 0;
        
        foreach(GameObject bee in bees)
        {
            if (bee != gameObject)
            {
                dist = Vector3.Distance(bee.transform.position, transform.position);
                if(dist <= Manager.neighbourDistance)
                {
                    cohesion += bee.transform.position;
                    numBees++;

                    if(dist < 1.0f)
                    {
                        separation += (transform.position - bee.transform.position);
                    }

                    Flock anotherBee = bee.GetComponent<Flock>();
                    align += anotherBee.speed;
                }
            }
        }
        if(numBees > 0)
        {
            cohesion /= numBees;
            speed = align / numBees;
            if(speed > Manager.maxSpeed)
            {
                speed = Manager.maxSpeed;
            }

            Vector3 direction = (cohesion + separation) - transform.position;
            if(direction !=Vector3.zero)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), Manager.rotationSpeed * Time.deltaTime);
            }
        }
    }

}

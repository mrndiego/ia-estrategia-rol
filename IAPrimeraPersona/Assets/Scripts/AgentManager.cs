using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentManager : MonoBehaviour
{
    public int NumRunners = 10;         // N�mero de FollowerRunners (y de GhostRunners)
    public int NumWanderers = 10;       // N�mero de Wanderers

    // Prefabs de los agentes
    public GameObject GhostRunnerPrefab;
    public GameObject FollowRunnerPrefab;
    public GameObject WandererPrefab;

    public Transform RunnersParent;     // Lugar del que saldr�n todos los Runners
    public Transform WanderersParent;   // Lugar del que saldr�n todos los Wanderers

    public Transform[] Route;           // Ruta que har�n los Runners

    public Transform[] Bancos;          // Bancos en los que se sientan los Wanderers

    void Start()
    {
        Transform[] InvertedRoute = new Transform[Route.Length];    // Creamos el array para la ruta invertida
        System.Array.Copy(Route, InvertedRoute, Route.Length);      // Copiamos la ruta original en el nuevo array
        System.Array.Reverse(InvertedRoute);                        // Invertimos el nuevo array

        for (int i = 0; i < NumRunners; i++)
        {
            //Crear el GhostRunner.
            GameObject TempGhostRunner = GameObject.Instantiate(GhostRunnerPrefab, RunnersParent);

            // Asignamos las rutas a los Runner. 50% de probabilidades de invertir la ruta original.
            if (Random.value > 0.5f) TempGhostRunner.GetComponent<Runner>().points = InvertedRoute; 
            else TempGhostRunner.GetComponent<Runner>().points = Route; 

            //Crear el FollowerRunner.
            GameObject TempFollowRunner = GameObject.Instantiate(FollowRunnerPrefab, RunnersParent);
            // Asignar a cada FollowerRunner su GhostRunner.
            TempFollowRunner.GetComponent<SeguidorLogica>().objetivo = TempGhostRunner.GetComponent<NavMeshAgent>();
        }
        for (int i = 0; i < NumWanderers; i++)
        {
            //Crear el Wanderer
            GameObject TempWanderer = GameObject.Instantiate(WandererPrefab, WanderersParent);
            //Asignar la lista de bancos a cada Wanderer.
            TempWanderer.GetComponent<WandererSM>().Bancos = Bancos;
        }
    }


}

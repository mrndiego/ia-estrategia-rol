using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WalkingState : BaseState   // Estado 'caminando'
{
    public override void Execute(WandererSM stateMachine)
    {
        if (!stateMachine.Agent.pathPending && stateMachine.Agent.remainingDistance < 0.5f)
        //Si el agente ha terminado de desplazarse y est� a menos de 0.5 unidades de distancia de su objetivo
        {
            if (!stateMachine.Descansado)   // Si el agente est� cansado, tiene que buscar bancos cerca de �l
            {
                float Dist = Mathf.Infinity;    // Variable auxiliar para guardar la distancia al banco m�s cercano, la inicializamos con el valor m�s grande posible
                Transform MasCercano = null;    // Referencia al banco m�s cercano.
                                                // Se puede inicializar a NULL sin problemas, ya que siempre se asigna al primer banco de la lista

                foreach (Transform b in stateMachine.Bancos) //Recorrer bancos[]
                {
                    float tempDist = Vector3.Distance(b.GetComponent<Transform>().localPosition, stateMachine.GetComponent<Transform>().position);
                    // Comprobamos la distancia del banco al agente
                    if (tempDist < Dist)    // Si la distancia es menor a la que ten�amos guardada
                    {
                        Dist = tempDist;    // Guardamos la nueva distancia
                        MasCercano = b;     // Actualizamos la referencia
                    }
                }

                if (Dist < 20.0f) //Si el banco m�s cercano est� a menos de 20 unidades de distancia
                {
                    stateMachine.Agent.SetDestination(MasCercano.position);  // Moverse al banco 
                    stateMachine.Transition(stateMachine.Resting);           // Cambiamos de estado
                }
                else
                {
                    Vector3 nextPos = GetRandomPosition(stateMachine);  // Obtenemos un punto aleatorio
                    stateMachine.Agent.SetDestination(nextPos);         // Fijamos el destino del agente al punto generado
                }
            }
            else    // Si el agente no est� cansado, busca un punto aleatorio al que desplazarse.
            {
                Vector3 nextPos = GetRandomPosition(stateMachine);  // Obtenemos el punto aleatorio
                stateMachine.Agent.SetDestination(nextPos);         // Fijamos el destino del agente al punto generado
                if (stateMachine.ContadorPasos < 5)                 // Si el agente ha hecho menos de 5 movimientos, no est� cansado
                {
                    stateMachine.ContadorPasos++;                   // Incrementamos el n�mero de movimientos
                }
                else                                                // Si el agente ha hecho 5 o m�s movimientos, est� cansado
                {
                    stateMachine.Descansado = false;                // Indicamos que el agente est� cansado
                }
                
            }
        }
    }

    private Vector3 GetRandomPosition(WandererSM stateMachine)  // M�todo auxiliar que devuelve una posici�n cercana aleatoria
    {
        Vector3 localTarget = UnityEngine.Random.insideUnitCircle * stateMachine.radius;    // Obtenemos un punto aleatorio en un c�culo del radio establecido
        localTarget += new Vector3(0, 0, stateMachine.offset);                              // Desplazamos ligeramente el c�rculo
        Vector3 worldTarget = stateMachine.transform.TransformPoint(localTarget);           // Pasamos las coordenadas del punto aleatorio a coordenadas en el mundo
        worldTarget.y = 0f;                                                                 // Fijamos la posici�n en el eje Y en 0
        return worldTarget;                                                                 // Retornamos el punto
    }
}

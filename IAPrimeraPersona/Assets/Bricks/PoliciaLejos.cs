using UnityEngine;

using Pada1.BBCore;
using Pada1.BBCore.Framework;

[Condition("Policia Lejos")]
public class PoliciaLejos : ConditionBase
{
    public override bool Check()
    {
        GameObject cop = GameObject.Find("Cop");
        GameObject treasure = GameObject.Find("Thief");
        return Vector3.Distance(cop.transform.position, treasure.transform.position) > 15f;
    }
}

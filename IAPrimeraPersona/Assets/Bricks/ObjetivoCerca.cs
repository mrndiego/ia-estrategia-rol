using UnityEngine;

using Pada1.BBCore;
using Pada1.BBCore.Framework;

[Condition("ObjetivoCerca")]
public class ObjetivoCerca : ConditionBase
{
    [InParam("Obj1")]
    public GameObject obj1;
    [InParam("Obj2")]
    public GameObject obj2;
    [InParam("Dist")]
    public float dist;
    public override bool Check()
    {
        return Vector3.Distance(obj1.transform.position, obj2.transform.position) < dist;
    }
}

using UnityEngine;

using Pada1.BBCore;
using Pada1.BBCore.Tasks;
using Pada1.BBCore.Framework;

[Action("MyActions/Steal")]
public class Steal : BasePrimitiveAction
{
    public override TaskStatus OnUpdate()
    {
        GameObject treasureManager = GameObject.Find("TreasureManager");
        if (treasureManager != null)
        {
            GameObject th = GameObject.Find("Thief");
            GameObject tr = GameObject.Find("Treasure");
            if(Vector3.Distance(th.transform.position, tr.transform.position) > 2 ) return TaskStatus.FAILED;
            if (treasureManager.GetComponent<TreasureManager>().Steal())
            {
                return TaskStatus.COMPLETED;
            }
        }
        return TaskStatus.FAILED;
    }
}

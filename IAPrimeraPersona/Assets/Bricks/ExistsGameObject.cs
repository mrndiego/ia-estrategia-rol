using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pada1.BBCore;
using Pada1.BBCore.Framework;

[Condition("ExistsGameObject")]
public class ExistsGameObject : ConditionBase
{
    public override bool Check()
    {
        GameObject treasure = GameObject.Find("Treasure");
        if (treasure != null)
            return treasure.active;
        else
            return false;
        
    }
}

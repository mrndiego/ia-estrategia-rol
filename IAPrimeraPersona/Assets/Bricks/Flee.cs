using UnityEngine;
using UnityEngine.AI;

using Pada1.BBCore;
using Pada1.BBCore.Tasks;
using Pada1.BBCore.Framework;

[Action("MyActions/Flee")]
public class Flee : BasePrimitiveAction
{
    NavMeshAgent navAgent;

    public override void OnStart()
    {
        GameObject th = GameObject.Find("Thief");
        GameObject cop = GameObject.Find("Cop");
        navAgent = th.GetComponent<NavMeshAgent>();

        Vector3 flee = cop.transform.position - th.transform.position;
        navAgent.SetDestination(th.transform.position - flee);

        #if UNITY_5_6_OR_NEWER
            navAgent.isStopped = false;
        #else
            navAgent.Resume();
        #endif
    }
    public override TaskStatus OnUpdate()
    {
        if (!navAgent.pathPending && navAgent.remainingDistance <= navAgent.stoppingDistance)
            return TaskStatus.COMPLETED;
        return TaskStatus.RUNNING;
    }
    }
